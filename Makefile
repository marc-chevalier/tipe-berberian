CC=g++
C11= -std=c++0x
FLAGSBASE= -O3 -W -Wextra -Wcast-qual -Wcast-align -Wfloat-equal -Wshadow -Wpointer-arith -Wunreachable-code -Wchar-subscripts -Wcomment -Wformat -Werror-implicit-function-declaration -Wmain -Wmissing-braces -Wparentheses -Wsequence-point -Wreturn-type -Wswitch -Wuninitialized -Wreorder -Wundef -Wshadow -Wwrite-strings -Wsign-compare -Wmissing-declarations 
NAZI= $(FLAGSBASE) -pedantic -Wconversion -Wmissing-noreturn -Wold-style-cast -Weffc++ -Wall -Wunused
CFLAGS=$(NAZI)
LDFLAGS= 
EXEC=setup sim sim_paralelle

all: $(EXEC) 

debug: CFLAGS = $(NAZI) -D DEBUG -g
debug: $(EXEC)

purge: clean all

clang: CC=clang++
clang: C11=-std=c++11
clang: $(EXEC)

setup:
	mkdir -p obj

sim: obj/sim.o obj/tools.o obj/save.o obj/init.o obj/lanceurs.o
	g++ -o sim obj/sim.o obj/tools.o obj/save.o obj/init.o obj/lanceurs.o

sim_paralelle: obj/sim_paralelle.o obj/tools.o obj/save.o obj/init.o obj/lanceurs.o
	g++ -o sim_paralelle obj/sim_paralelle.o obj/tools.o obj/save.o obj/init.o obj/lanceurs.o -pthread -std=c++0x -lpthread

obj/sim_paralelle.o:  sim_paralelle.cpp
	g++ -o obj/sim_paralelle.o -c sim_paralelle.cpp -std=c++0x -O3 -Wextra -Wall -Wfloat-equal -Wshadow -Wunused -Wunreachable-code -pthread -lpthread -pedantic $(NAZI)

obj/sim.o:  sim.cpp
	g++ -o obj/sim.o -c sim.cpp -std=c++0x -O3 -Wextra -Wall -Wfloat-equal -Wshadow -Wunused -Wunreachable-code -pedantic $(NAZI)

obj/tools.o:  tools.cpp
	g++ -o obj/tools.o -c tools.cpp -std=c++0x -O3 -Wextra -Wall -Wfloat-equal -Wshadow -Wunused -Wunreachable-code -pedantic $(NAZI)
	
obj/save.o:  save.cpp
	g++ -o obj/save.o -c save.cpp -std=c++0x -O3 -Wextra -Wall -Wfloat-equal -Wshadow -Wunused -Wunreachable-code -pedantic $(NAZI)
	
obj/init.o:  init.cpp
	g++ -o obj/init.o -c init.cpp -std=c++0x -O3 -Wextra -Wall -Wfloat-equal -Wshadow -Wunused -Wunreachable-code -pedantic $(NAZI)
	
obj/lanceurs.o:  lanceurs.cpp
	g++ -o obj/lanceurs.o -c lanceurs.cpp -std=c++0x -O3 -Wextra -Wall -Wfloat-equal -Wshadow -Wunused -Wunreachable-code -pedantic $(NAZI)

clean:
	rm -f obj/*.o
