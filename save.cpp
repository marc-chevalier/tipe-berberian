#include "save.h"
#include <fstream>

using namespace std;

void saveV(int n, int m, string path, string fileVName, string geometrieName, string ext, const vector<vector<vector<double>>>& U)
{
    ofstream fileV(path+fileVName+geometrieName+ext);

    fileV<<2*n+1<<" "<<m+1<<" "<<2*n+1<<" ";

    for(int i=0; i<2*n+1; ++i)
        for(int j=0; j<m+1; ++j)
            for(int k=0; k<2*n+1; ++k)
                fileV<<U[i][j][k]<<" ";

    fileV.close();
}

void saveEquipo(int nbEquipotentielles, string path, string fileEquipoName, string geometrieName, string ext, const vector<vector<pair<int, int>>>& equipo)
{
    ofstream fileEquipo(path+fileEquipoName+geometrieName+ext);

    fileEquipo<<nbEquipotentielles<<" ";

    for(unsigned int p=0; p<equipo.size(); ++p)
    {
        for(unsigned int i=0; i<equipo[p].size(); ++i)
            fileEquipo<<equipo[p][i].first<<" "<<equipo[p][i].second<<" ";

        fileEquipo<<-1<<" "<<-1<<" ";
    }

    fileEquipo.close();
}
