#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <sstream>
#include <thread>
#include "tools.h"
#include "save.h"
#include "init.h"
#include "lanceurs.h"

using namespace std;

void calculUSecu();
void calculU();
void calculEquipo();
void save();

double b = 1000.;                   ///Commun
double d = 100.;                    ///Commun
int n = 10;                         ///Commun
int m = 50;                         ///Commun
double V0 = 20000;                  ///Commun
int iter_max = 2000;                ///Commun
double precision = 0.001;           ///Commun
int tranche = 0;                    ///Commun
int nbEquipotentielles = 1;         ///Commun
vector<double> equi(0);             ///Commun
string fileVName="fileV";           ///Commun
string fileEquipoName="fileEquipo"; ///Commun
string path = "C:/TIPE Berberian/"; ///Commun
string ext = ".txt";                ///Commun
int nb_config = 1;                  ///Commun

int ep = 3;                         ///Specificite geometrique
int h = 10;                         ///Specificite geometrique
int r = 1;                          ///Specificite geometrique
int H = 10;                         ///Specificite geometrique
int R = 1;                          ///Specificite geometrique
double precisionAngulaire = 1;      ///Specificite geometrique
int dx=0;
int dy=0;
int dz=0;

string geometrieName="_pave_droit"; ///Specificite geometrique
string geometrie="PaveDroit";

string configFile = "config.txt";
vector<vector<vector<double> > > U;
vector<vector<vector<double> > > UU;
vector<vector<pair<int,int> > > equipo;
vector<vector<vector<bool> > > calcul;

void calculUSecu()
{
    UU = U;

    calcul = initSecu(U, precision, n, m);

    for(int iter=0; iter<iter_max; ++iter)
    {
        for(int i=1; i<2*n; ++i)
            for(int j=1; j<m; ++j)
                for(int k=1; k<2*n; ++k)
                    if(calcul[i][j][k])
                        UU[i][j][k] = (4*n*n*(U[i-1][j][k]+U[i+1][j][k])/(b*b)+m*m*(U[i][j+1][k]+U[i][j-1][k])/(d*d)+4*n*n*(U[i][j][k+1]+U[i][j][k-1])/(b*b))/(16*n*n/(b*b)+2*m*m/(d*d));
        U=UU;
    }
}

void calculU()
{
    UU = U;

    for(int iter=0; iter<iter_max; ++iter)
    {
        cout<<iter<<" ";
        for(int i=1; i<2*n; ++i)
            for(int j=1; j<m; ++j)
                for(int k=1; k<2*n; ++k)
                    UU[i][j][k] = (4*n*n*(U[i-1][j][k]+U[i+1][j][k])/(b*b)+m*m*(U[i][j+1][k]+U[i][j-1][k])/(d*d)+4*n*n*(U[i][j][k+1]+U[i][j][k-1])/(b*b))/(16*n*n/(b*b)+2*m*m/(d*d));
        U=UU;
    }
}


void calculEquipo()
{
    for(int p=0; p<nbEquipotentielles; ++p)
        for(int i=0; i<2*n+1; ++i)
        {
            int id=0;
            for(int j=0; j<m; ++j)
                if(abs(equi[p]-U[i][id][tranche])>abs(equi[p]-U[i][j][tranche]))
                    id=j;
            equipo[p].push_back(pair<int, int>(i, id));
        }
}

void save()
{
    saveV(n, m, path, fileVName, geometrieName, ext, U);
    saveEquipo(nbEquipotentielles, path, fileEquipoName, geometrieName, ext, equipo);
}

int main()
{
    ifstream config(configFile);

    config>>b;
    config>>d;
    config>>n;
    config>>m;
    config>>V0;
    config>>iter_max;
    config>>precision;
    config>>tranche;
    config>>nbEquipotentielles;
    equi.resize(nbEquipotentielles,0);
    for(int i=0; i<nbEquipotentielles; ++i)
    {
        config>>equi[i];
    }
    config.ignore();
    //config.ignore();
    getline(config,fileVName);
    fileVName=clean(fileVName);
    //fileVName=fileVName.substr(0,fileVName.size()-1);
    getline(config,fileEquipoName);
    fileEquipoName=clean(fileEquipoName);
    //fileEquipoName=fileEquipoName.substr(0,fileEquipoName.size()-1);
    getline(config,path);
    path=clean(path);
    //path=path.substr(0,path.size()-1);
    getline(config,ext);
    ext=clean(ext);
    //ext=ext.substr(0,ext.size()-1);

    U=vector<vector<vector<double> > >(2*n+1, vector<vector<double> >(m+1, vector<double>(2*n+1,0)));
    UU=vector<vector<vector<double> > >(2*n+1, vector<vector<double> >(m+1, vector<double>(2*n+1,0)));
    equipo=vector<vector<pair<int,int> > >(nbEquipotentielles,vector<pair<int,int> >(0));

    config>>nb_config;
    config.ignore();
    initEspaceVide(U, n, m, V0);

    for(int i=0;i<nb_config;++i)
    {
        getline(config,geometrie);
        geometrie=clean(geometrie);
        //geometrie=geometrie.substr(0,geometrie.size()-1);
        if(geometrie.substr(0,9)=="PaveDroit")
            lanceurPaveDroit(geometrieName, config, U, h, ep, n, dx, dy, dz);
        else if(geometrie.substr(0,10)=="EspaceVide")
            lanceurEspaceVide(geometrieName, U, n, m, V0);
        else if(geometrie.substr(0,11)=="ConeTronque")
            lanceurConeTronque(geometrieName, config, U, precisionAngulaire, n, H, R, r, dx, dy, dz);
        else if(geometrie.substr(0,4)=="Cone")
            lanceurCone(geometrieName, config, U, precisionAngulaire, n, h, r, dx, dy, dz);
    }

    if(nb_config>1)
        geometrieName="_composite";

    config.close();

    calculUSecu();


    calculEquipo();
    save();

    return 0;
}
