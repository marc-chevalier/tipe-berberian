#include "tools.h"

using namespace std;

string clean(string e)
{
    string s="";

    for(unsigned int i=0;i<e.size();++i)
    {
        if(e[i]<='z' && e[i]>='a')
            s+=e[i];
        else if(e[i]<='Z' && e[i]>='A')
            s+=e[i];
        else if(e[i]<='9' && e[i]>='0')
            s+=e[i];
        else if(e[i]=='\\' || e[i]=='/' ||e[i]=='.' || e[i] == '_' || e[i]=='-' || e[i]=='(' || e[i]==')')
            s+=e[i];
        else if(e[i]>=32)
            s+=e[i];
    }

    return s;
}

