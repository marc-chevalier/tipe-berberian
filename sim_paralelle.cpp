#include <iostream>
#include <vector>
#include <cmath>
#include <fstream>
#include <sstream>
#include <thread>
#include "tools.h"
#include "save.h"
#include "init.h"
#include "lanceurs.h"

using namespace std;

void calculEquipo();
void save();
void iterUBorne(int iMin, int iMax, int jMin, int jMax, int kMin, int kMax);
void calculUSecuParallele();
void calculUneEquipo(int p);

int nb_thread = 1;                  ///Commun
double b = 1000.;                   ///Commun
double d = 100.;                    ///Commun
int n = 10;                         ///Commun
int m = 50;                         ///Commun
double V0 = 20000;                  ///Commun
int iter_max = 2000;                ///Commun
double precision = 0.001;           ///Commun
int tranche = 0;                    ///Commun
int nbEquipotentielles = 1;         ///Commun
vector<double> equi(0);             ///Commun
string fileVName="fileV";           ///Commun
string fileEquipoName="fileEquipo"; ///Commun
string path = "C:/TIPE Berberian/"; ///Commun
string ext = ".txt";                ///Commun
int nb_config = 1;                  ///Commun

int ep = 3;                         ///Specificite geometrique
int h = 10;                         ///Specificite geometrique
int r = 1;                          ///Specificite geometrique
int H = 10;                         ///Specificite geometrique
int R = 1;                          ///Specificite geometrique
double precisionAngulaire = 1.;      ///Specificite geometrique
int dx=0;
int dy=0;
int dz=0;

string geometrieName=""; ///Specificite geometrique
string geometrie="";

string configFile = "config_paralelle.txt";
vector<vector<vector<double> > > U;
vector<vector<vector<double> > > UU;
vector<vector<pair<int,int> > > equipo;
vector<vector<vector<bool> > > calcul;

void iterUBorne(int iMin, int iMax, int jMin, int jMax, int kMin, int kMax)
{
    for(int i=max(iMin,1); i<min(iMax,2*n); ++i)
        for(int j=max(jMin,1); j<min(jMax,m); ++j)
            for(int k=max(kMin,1); k<min(kMax,2*n); ++k)
            {
                if(calcul[i][j][k])
                    UU[i][j][k] = (4*n*n*(U[i-1][j][k]+U[i+1][j][k])/(b*b)+m*m*(U[i][j+1][k]+U[i][j-1][k])/(d*d)+4*n*n*(U[i][j][k+1]+U[i][j][k-1])/(b*b))/(16*n*n/(b*b)+2*m*m/(d*d));
            }
}

void calculUSecuParallele()
{
    UU=U;
    calcul = initSecu(U, precision, n, m);
    vector<thread*> th;

    for(int iter=0; iter<iter_max;++iter)
    {
        cout<<iter<<endl;
        th.clear();
        for(int i=0;i<nb_thread;++i)
        {
            th.push_back(new thread(iterUBorne,(2*n*i)/nb_thread,(2*n*(i+1))/nb_thread,1,m,1,2*n));
        }

        for(int i=0;i<nb_thread;++i)
            th[i]->join();
        for(int i=0;i<nb_thread;++i)
            delete th[i];
        U=UU;
    }
}

void calculUneEquipo(int p)
{
    for(int i=0; i<2*n+1; ++i)
    {
        int id=0;
        for(int j=0; j<m; ++j)
        {
            if(abs(equi[p]*V0-U[i][id][tranche])>=abs(equi[p]*V0-U[i][j][tranche]))
            {
                id=j;
            }
        }

        equipo[p].push_back(pair<int, int>(i, id));
    }
}

void calculEquipo()
{
    vector<thread*> th;
    for(int p=0; p<nbEquipotentielles; ++p)
        th.push_back(new thread(calculUneEquipo,p));

    for(int p=0; p<nbEquipotentielles; ++p)
        th[p]->join();

    for(int p=0; p<nbEquipotentielles; ++p)
        delete th[p];

    th.clear();
}

void save()
{
    //thread threadSaveV(saveV, n, m, path, fileVName, geometrieName, ext, U);
    thread threadSaveEquipo(saveEquipo, nbEquipotentielles, path, fileEquipoName, geometrieName, ext, equipo);

    //threadSaveV.join();
    threadSaveEquipo.join();
}

int main()
{
    ifstream config(configFile);

    config>>nb_thread;
    config>>b;
    config>>d;
    config>>n;
    config>>m;
    config>>V0;
    config>>iter_max;
    config>>precision;
    config>>tranche;
    config>>nbEquipotentielles;
    cout<<nbEquipotentielles<<endl;
    equi.resize(nbEquipotentielles,0);
    for(int i=0; i<nbEquipotentielles; ++i)
        config>>equi[i];

    config.ignore();
    config.ignore();
    getline(config,fileVName);
    cout<<fileVName<<endl;
    cout<<fileVName.size()<<endl;
    //fileVName=fileVName.substr(0,fileVName.size()-1);
    fileVName=clean(fileVName);
    getline(config,fileEquipoName);
    //fileEquipoName=fileEquipoName.substr(0,fileEquipoName.size()-1);
    fileEquipoName=clean(fileEquipoName);
    getline(config,path);
    //path=path.substr(0,path.size()-1);
    path=clean(path);
    getline(config,ext);
    ext=clean(ext);
    //ext=ext.substr(0,ext.size()-1);

    U=vector<vector<vector<double> > >(2*n+1, vector<vector<double> >(m+1, vector<double>(2*n+1,0)));
    UU=vector<vector<vector<double> > >(2*n+1, vector<vector<double> >(m+1, vector<double>(2*n+1,0)));
    equipo=vector<vector<pair<int,int> > >(nbEquipotentielles,vector<pair<int,int> >(0));

    config>>nb_config;
    config.ignore();
    config.ignore();
    initEspaceVide(U, n, m, V0);
    cout<<nb_config<<" "<<m<<endl;

    for(int i=0;i<nb_config;++i)
    {
        getline(config,geometrie);
        //geometrie=geometrie.substr(0,geometrie.size()-1);
        cout<<geometrie<<endl;
        cout<<geometrie.size()<<endl;
        if(geometrie.substr(0,9)=="PaveDroit")
            lanceurPaveDroit(geometrieName, config, U, h, ep, n, dx, dy, dz);
        else if(geometrie.substr(0,10)=="EspaceVide")
            lanceurEspaceVide(geometrieName, U, n, m, V0);
        else if(geometrie.substr(0,11)=="ConeTronque")
            lanceurConeTronque(geometrieName, config, U, precisionAngulaire, n, H, R, r, dx, dy, dz);
        else if(geometrie.substr(0,4)=="Cone")
            lanceurCone(geometrieName, config, U, precisionAngulaire, n, h, r, dx, dy, dz);
    }

    if(nb_config>1)
        geometrieName="_composite";

    config.close();

    calculUSecuParallele();

    calculEquipo();
    save();

    return 0;
}
