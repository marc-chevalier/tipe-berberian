#ifndef SAVE
#define SAVE

#include<string>
#include<vector>

void saveV(int n, int m, std::string path, std::string fileVName, std::string geometrieName, std::string ext, const std::vector<std::vector<std::vector<double>>>& U);
void saveEquipo(int nbEquipotentielles, std::string path, std::string fileEquipoName, std::string geometrieName, std::string ext, const std::vector<std::vector<std::pair<int, int>>>& equipo);

#endif
