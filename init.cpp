#include "init.h"
#include<cmath>
#define PI 3.14159265358979323846

using namespace std;

void initEspaceVide(vector<vector<vector<double>>>& U, int n, int m, double V0)
{
    for(int i=0; i<2*n+1; ++i)
        for(int j=0; j<m+1; ++j)
            for(int k=0; k<2*n+1; ++k)
                U[i][j][k] = V0*(j-1)/m;
}

void initPaveDroit(vector<vector<vector<double>>> &U, int h, int ep, int n, int dx, int dy, int dz)
{
    for(int j=0; j<h; ++j)
        for(int i=n+1-ep; i<n+1+ep; ++i)
            for(int k=n+1-ep; k<n+1+ep; ++k)
                U[i+dx][j+dy][k+dz]=0;
}

void initCone(vector<vector<vector<double>>>& U, double precisionAngulaire, int n, int h, int r, int dx, int dy, int dz)
{
    double R_;
    for(int j=0; j<h; ++j)
    {
        R_=r*(h-j)/h;
        for (double theta=0; theta<360; theta+=precisionAngulaire)
            for (int i=static_cast<int>(-abs(static_cast<int>(R_*cos(theta*PI/180)+0.5))+n+1); i<abs(static_cast<int>(R_*cos(theta*PI/180)+0.5))+n+1; ++i)
                for (int k=static_cast<int>(-abs(static_cast<int>(R_*sin(theta*PI/180)+0.5))+n+1); k<abs(static_cast<int>(R_*sin(theta*PI/180)+0.5))+n+1; ++k)
                    U[i+dx][j+dy][k+dz]=0;
    }

}

void initConeTronque(vector<vector<vector<double>>>& U, double precisionAngulaire, int n, int H, int R, int r, int dx, int dy, int dz)
{
    double t;
    for(int j=0; j<H; ++j)
    {
        t=((H-j)*R+(j*r))/H;
        for (double theta=0; theta<360; theta+=precisionAngulaire)
            for (int i=static_cast<int>(-abs(static_cast<int>(t*cos(theta*PI/180)+0.5))+n+1); i<abs(static_cast<int>(t*cos(theta*PI/180)+0.5))+n+1; ++i)
                for (int k=static_cast<int>(-abs(static_cast<int>(t*sin(theta*PI/180)+0.5))+n+1); k<abs(static_cast<int>(t*sin(theta*PI/180)+0.5))+n+1; ++k)
                    U[i+dx][j+dy][k+dz]=0;
    }

}

vector<vector<vector<bool> > > initSecu(vector<vector<vector<double>>>& U, double precision, int n, int m)
{
    vector<vector<vector<bool> > > calcul(2*n+1,vector<vector<bool> >(m+1,vector<bool>(2*n+1,false)));

    for(int i=0; i<2*n+1; ++i)
        for(int j=0; j<m+1; ++j)
            for(int k=0; k<2*n+1; ++k)
                if(U[i][j][k]>precision)
                    calcul[i][j][k]=true;
    return calcul;
}
