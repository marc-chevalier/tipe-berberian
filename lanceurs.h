#ifndef LANCEURS_CPP_INCLUDED
#define LANCEURS_CPP_INCLUDED

#include<fstream>
#include<vector>

void lanceurEspaceVide(std::string& geometrieName, std::vector<std::vector<std::vector<double>>>& U, int n, int m, double V0);
void lanceurPaveDroit(std::string& geometrieName, std::ifstream& config, std::vector<std::vector<std::vector<double>>> &U, int h, int ep, int n, int dx, int dy, int dz);
void lanceurCone(std::string& geometrieName, std::ifstream& config, std::vector<std::vector<std::vector<double>>>& U, double precisionAngulaire, int n, int h, int r, int dx, int dy, int dz);
void lanceurConeTronque(std::string& geometrieName, std::ifstream& config, std::vector<std::vector<std::vector<double>>>& U, double precisionAngulaire, int n, int H, int R, int r, int dx, int dy, int dz);

#endif // LANCEURS_CPP_INCLUDED
