#include "lanceurs.h"
#include "init.h"
#include <iostream>

using namespace std;

void lanceurEspaceVide(string& geometrieName, vector<vector<vector<double>>>& U, int n, int m, double V0)
{
    geometrieName="_espace_vide";
    initEspaceVide(U, n, m, V0);
}

void lanceurPaveDroit(string& geometrieName, ifstream& config, vector<vector<vector<double>>> &U, int h, int ep, int n, int dx, int dy, int dz)
{
    cout<<"Pave droit"<<endl;
    config>>dx;
    config>>dy;
    config>>dz;
    config>>ep;
    config>>h;
    geometrieName="_pave_droit";
    initPaveDroit(U, h, ep, n, dx, dy, dz);
}

void lanceurCone(string& geometrieName, ifstream& config, vector<vector<vector<double>>>& U, double precisionAngulaire, int n, int h, int r, int dx, int dy, int dz)
{
    cout<<"Cone"<<endl;
    config>>dx;
    config>>dy;
    config>>dz;
    config>>r;
    config>>h;
    config>>precisionAngulaire;
    geometrieName="_cone";
    initCone(U, precisionAngulaire, n, h, r, dx, dy, dz);
}

void lanceurConeTronque(string& geometrieName, ifstream& config, vector<vector<vector<double>>>& U, double precisionAngulaire, int n, int H, int R, int r, int dx, int dy, int dz)
{
    cout<<"Cone tronque"<<endl;
    config>>dx;
    config>>dy;
    config>>dz;
	config>>R;
	config>>r;
	config>>H;
	config>>precisionAngulaire;
	geometrieName="_cone_tronque";
	initConeTronque(U, precisionAngulaire, n, H, R, r, dx, dy, dz);
}
