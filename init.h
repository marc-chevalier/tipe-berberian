#ifndef INIT_H_INCLUDED
#define INIT_H_INCLUDED

#include<vector>

void initEspaceVide(std::vector<std::vector<std::vector<double>>>& U, int n, int m, double V0);
void initPaveDroit(std::vector<std::vector<std::vector<double>>>& U, int h, int ep, int n, int dx, int dy, int dz);
void initCone(std::vector<std::vector<std::vector<double>>>& U, double precisionAngulaire, int n, int h, int r, int dx, int dy, int dz);
void initConeTronque(std::vector<std::vector<std::vector<double>>>& U, double precisionAngulaire, int n, int H, int R, int r, int dx, int dy, int dz);
std::vector<std::vector<std::vector<bool> > > initSecu(std::vector<std::vector<std::vector<double>>>& U, double precision, int n, int m);

#endif // INIT_H_INCLUDED
